import random
import string
from datetime import datetime

import requests
from requests.auth import HTTPBasicAuth

from .fake_server import FakeServer
from .myapp import Client, Little, execute_queries
from unittest import mock, TestCase

"""
Assume that we can't access outside URLs. How can we make the tests use `FakeServer`?

Tasks:

1. Test that the Client will authenticate with Basic Auth
2. Test that the Client will perform a query that returns a response
3. Test that `execute_queries` will return a list of responses of correct length

"""


def do_client_query(server: FakeServer) -> requests.Response:
    return requests.post(url=f"http://0.0.0.0:{server.mock_server_port}")


# will be nice a mark test-class as test-class (and for use batch command like python3 -m unittest -v TestProject.test)
class TestClient(TestCase):
    server = FakeServer()

    @mock.patch.object(Little, "URL", f"http://localhost:{server.mock_server_port}")
    def test_client_authenticates_w_fake_server(self) -> None:
        # TODO: authenticate with FakeServer to test Client
        client = Client("fakeuser", "fakesecret")
        assert client.authenticate()

    @mock.patch.object(Little, "URL", f"http://localhost:{server.mock_server_port}")
    def test_client_query_w_fake_server(self) -> None:
        # TODO: query the FakeServer to test Client
        client = Client("fakeuser", "fakesecret")
        response = client.query(params={"param1": "value"})
        assert response.status_code == 200
        assert isinstance(response.json()["result"], list)
    

class TestExecuteQueries(TestCase):
    server = FakeServer()

    @mock.patch.object(Little, "URL", f"http://localhost:{server.mock_server_port}")
    def test_execute_many_queries_works(self):
        # TODO: make many queries with FakeServer to test Client
        num_queries = 50
        queries = [{f"param{num}": f"value{num}"} for num in range(num_queries)]
        results = execute_queries(queries)
        assert isinstance(results, list)
        assert len(results) == num_queries


class TestFakeServer(TestCase):
    def test_set_201(self):
        with FakeServer(status=201) as server:
            response = do_client_query(server)
            assert response.status_code == 201

    def test_set_payload(self):
        payload = {"item": "test item in here"}

        with FakeServer(payload=payload) as server:
            response = do_client_query(server)
            assert response.status_code == 200
            assert response.json() == payload

    def test_basic_auth(self):
        """test if basic http auth returns a token"""
        with FakeServer() as server:
            request = requests.Request(
                url=f"http://0.0.0.0:{server.mock_server_port}/auth")
            request.method = "POST"
            request.auth = HTTPBasicAuth("myuser", "mypassword")
            response = requests.session().send(request.prepare())
            assert "token" in response.json()
            assert len(response.json()["token"]) == 64

    def test_fake_query(self):
        with FakeServer() as server:
            token = "".join([random.choice(string.ascii_letters) for num in range(64)])

            # force auth
            server.mock_server.RequestHandlerClass.token = token
            server.mock_server.RequestHandlerClass.token_time = datetime.now()

            request = requests.Request(
                url=f"http://0.0.0.0:{server.mock_server_port}/query")
            request.method = "GET"
            request.headers.update({"Authorization": f"Bearer {token}"})
            response = requests.session().send(request.prepare())
            assert "result" in response.json()
            assert isinstance(response.json()["result"], list)
