### Tasks

Complete TODO items in `myapp.py` and `tests.py`.


###### TODO `myapp.py`
* Complete two functions - `authenticate` and `_pre_query`

1. `authenticate` - contact `Little` class-based API URL and retrieve a token with a 
username and password. This will involve a `POST` request with the appropriate header. 
Note - the `ApiProvider` and URLs are not real - it's a toy class for the test.

2. `_pre_query` - Determine if we need to call the `authenticate` method, maybe for 
token timeout or no token yet. 

###### TODO `tests.py`
* Finish three tests to a passing state. The tests are marked with `# TODO`. You can 
 make them work by by ensuring the `Client` class works with our `FakeServer`...