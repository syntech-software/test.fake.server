import base64
import json
import random
import socket
import string
import traceback
from datetime import datetime, timedelta
from http.server import BaseHTTPRequestHandler, HTTPServer
from threading import Thread
from typing import Optional

"""
Pretend there is a real life auth flow like:
 - Basic HTTP auth --> Token returned in payload like: {"token": <token here>}
 - Query with token in "Authorization" header like "Authorization: Bearer <some_token>"
 - Tokens expire every 15 minutes
"""

"""
Usage:

def do_client_query(server: FakeServer) -> requests.Response:
    return requests.post(url=f"http://localhost:{server.mock_server_port}")

with FakeServer(status=500) as server:
    assert do_client_query(server=server)).status_code == 500
"""


class MockServerRequestHandler(BaseHTTPRequestHandler):
    """
    You can subclass this thing easily - just add your logic to `do_GET` or `do_POST`,
      or just subclass your own BaseHTTPRequestHandler
    """

    status = None
    payload = None
    token = None
    token_time = None

    TOKEN_TIMEOUT = timedelta(minutes=15)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def default_process(self, payload: dict) -> None:
        """send a response with a given status/payload"""
        self.send_response(self.status or 200)
        if payload is None:
            self.end_headers()
            return
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps(payload).encode("utf-8"))
        return

    def get_auth_string(self) -> Optional[str]:
        """if there is an Authorization header, return its value"""
        for header in self.headers._headers:
            if "authorization" in header[0].lower():
                return header[1].strip()
        return None

    def basic_is_valid(self) -> bool:
        """check if a request conforms to the most basic of http basic auth protocol"""
        auth_string = self.get_auth_string()
        if auth_string is None:
            return False

        if "basic" in auth_string.lower():
            seq = auth_string.split(" ")[1]
            dec = base64.b64decode(seq)
            if ":" in str(dec):
                return True
        return False

    def token_is_valid(self) -> bool:
        """check if token is valid by looking at issued token and datetime of issue"""
        auth_string = self.get_auth_string()
        if auth_string is None:
            return False

        if "bearer" in auth_string.lower():
            token = auth_string.split(" ")[1]
        else:
            return False

        if self.token is None or self.token_time is None:
            return False
        elif self.token_time >= datetime.now() + self.TOKEN_TIMEOUT:
            return False
        elif self.token != token:
            return False
        return True

    def create_token(self) -> str:
        """create a 64-char token and store it with datetime of issue"""
        # when you write `server.mock_server.RequestHandlerClass.token` - you update static propery of class
        # and you call read it in each instance of this class
        # when you write `self.token` - you write only current instance of class and can be read only in it
        # and, how you of cource know, HTTPServer create a new instance of RequestHeaders at each query
        self.__class__.token = "".join([random.choice(string.ascii_letters) for num in range(64)])
        self.__class__.token_time = datetime.now()
        return self.token

    def do_GET(self):
        if self.path.endswith("auth") or self.path.endswith("auth/"):
            self.send_response(405)
            self.end_headers()
            return
        # Request.path containe path and query string, and if GET-params existed, Request.path newer ends on 'query'
        elif self.path.startswith("/query"):
            if not self.token_is_valid():
                self.send_response(401)
                self.end_headers()
                return
            return self.default_process(
                payload={"result": [{f"item{i}": f"value{i}"} for i in range(3)]}
            )
        else:
            return self.default_process(payload=self.payload)

    def do_POST(self):
        if self.path.endswith("query") or self.path.endswith("query/"):
            self.send_response(405)
            self.end_headers()
            return
        elif self.path.endswith("auth") or self.path.endswith("auth/"):
            if not self.basic_is_valid():
                self.send_response(403)
                self.end_headers()
                return
            return self.default_process(
                payload={"token": self.create_token()}
            )
        else:
            return self.default_process(payload=self.payload)


def get_free_port():
    s = socket.socket(socket.AF_INET, type=socket.SOCK_STREAM)
    s.bind(('localhost', 0))
    address, port = s.getsockname()
    s.close()
    return port


def get_server_class(status=None, payload=None):
    klass = MockServerRequestHandler
    klass.status = status
    klass.payload = payload
    return klass


class FakeServer(object):
    def __init__(self, status=None, payload=None):
        # Configure mock server.
        self.mock_server_port = get_free_port()

        self.mock_server = HTTPServer(('127.0.0.1', self.mock_server_port),
                                      get_server_class(status, payload))

        self.mock_server_thread = Thread(target=self.mock_server.serve_forever)
        self.mock_server_thread.setDaemon(True)
        self.mock_server_thread.start()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is not None:
            traceback.print_exception(exc_type, exc_value, tb)
            return False
        return True
