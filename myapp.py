import base64
import requests
from typing import Optional

"""
Tasks:

1. Write the authentication function for Client.
2. Check if we need to authenticate before we execute query
"""


class ApiProvider(object):
    """
    Imagine this class has some kind of utility -- it's not pointless.
    """

    URL = "https://api.ApiProvider.com"
    TEST_URL = "https://test-api.ApiProvider.com"

    def __init__(self, test: bool = False) -> None:
        if test:
            self.url = self.TEST_URL
        else:
            self.url = self.URL


class Little(object):
    URL = 'http://sample.api'
    token = None

    def _auth(self, auth_string):
        request = requests.Request(url=f"{self.URL}/auth", method="POST", headers={'Authorization': auth_string})
        response = requests.session().send(request.prepare())
        if response.status_code == 200:
            self.token = response.json().get('token')
            if self.token:
                return True
        return False

    @property
    def _auth_header(self):
        return {'Authorization': f"Bearer {self.token}"}

    def _is_token(self):
        if self.token:
            request = requests.Request(url=f"{self.URL}/query", method="GET", headers=self._auth_header)
            response = requests.session().send(request.prepare())
            if response.status_code == 401:
                return False
            else:
                return True
        return False

    @property
    def url(self):
        return f"{self.URL}/query"


class Client(object):
    """
    Very basic class that performs a query with an ApiProvider.

    Usage:
    ```
    client = Client()
    response = client.query( {"param1": "value1"} )
    print( response.json() )
    ```
    """

    TOKEN_TIMEOUT = (15 * 60) - 30  # tokens are only valid for 15 minutes

    def __init__(self, client_id: str, client_secret: str) -> None:
        self.api = ApiProvider()
        # auth_string was wrong - "Basic" should not be base64-encoded. And str->byte conversion missed
        b64_string = base64.b64encode(f"{client_id}:{client_secret}".encode()).decode()
        self.auth_string = f"Basic {b64_string}"

    def authenticate(self) -> bool:
        """return True if success, else False"""
        # TODO: HTTP basic auth with self.api
        if not isinstance(self.api, Little):
            self.api = Little()
        return  self.api._auth(self.auth_string)
    
    def _pre_query(self) -> None:
        # TODO: authenticate if needed
        if not isinstance(self.api, Little):
            self.api = Little()

        if not self.api._is_token():
            self.api._auth(self.auth_string)

    
    def query(self, params: dict) -> Optional[requests.Response]:
        """no touch for creating tests"""
        self._pre_query()

        # fictional API only takes strings
        params = {k: v for k, v in params.items() if isinstance(v, str)}

        # you must add some credentail to request in any way
        response = requests.get(url=self.api.url, params=params, headers=self.api._auth_header)
        if response.status_code != 200:
            return None
        return response


def execute_queries(queries: list) -> list:
    client_id = "fake"  # pretend these come from environ
    client_secret = "more_fake"  # pretend these come from environ
    client = Client(client_id, client_secret)
    results = [client.query(q) for q in queries]
    filtered_results = [r for r in results if r is not None]
    return filtered_results
